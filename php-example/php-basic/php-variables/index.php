<?php

// đặt tên biến theo camelCase
// Được bắt đầu bằng ký tự $
$tenToiLa = " Huy";
// Tên biến phân biệt chữ hoa và chữ thường
$age = 23;
$AGE = 22;

// Có thể gán giá trị cho biến hoặc nhiều biến cùng một lúc
//$motGiaTri = "Toi la Huy 1";
//$giaTri1 = $giaTri2 = $giaTri3 = "Toi la Huy 2";
//echo $motGiaTri;
//echo $giaTri1;
//echo $giaTri2;
//echo "\n" . $giaTri3;

/*
 * Scope trong PHP
 * Các phạm vi truy cập của biến: global, local
 * */

/*
 * Global
 * */
//$x = 5;
//function test() {
//    echo "<p>Gia tri x la: $x</p>";
//}
//test();
//
//echo "<p>Goa tri x la: $x</p>";

/*
 * Local
 * */
//function myTest() {
//    $x = 5; // local scope
//    echo "<p>Gia tri la: $x</p>";
//}
//myTest();
//echo "<p>Gia tri la: $x</p>";

/*
 * Truyền tham chiếu với biến
 * + Chỉ có các biến được đặt tên mới có thể gán bằng tham chiếu
 * + Truyền tham chiếu sử dụng dấu &
 * */
//$foo = 'Bob';
//$bar = &$foo;
//$bar = "My name is $bar";
//echo $bar;
//echo $foo;

/*
 * Variable variables
 * + Sử dung giá trị của một biến làm tên biến cho một biến khác
 * */
//$Bar = "a";
//$Foo = "Bar";
//$World = "Foo";
//$Hello = "World";
//$a = "Hello";
//
//echo $a;
//echo $$a;
//echo $$$a;
