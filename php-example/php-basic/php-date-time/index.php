<?php

/*
 * date_default_timezone_set: Cau hinh thoi gian theo tung vung
 * */
//date_default_timezone_set("Asia/Ho_Chi_Minh");

/*
 * getDate() trả về mảng chứa thông tin về ngày giờ hiện tại hoặc thời gian được cung cấp
 * getDate(timestamp)
*/
//$getDate = getdate();
//print_r($getDate);

/*
 * date() định dạng ngày và giờ theo mot dinh dang cu the
 * date(dinh dang hien thi, timestamp)
 * */
//print_r(date("Y-m-d H:i:s"));

/*
 * time tra ve ket qua tinh bang giay
 * */
//$currentTimestamp = time();
//echo $currentTimestamp;

/*
 * strtotime : dùng để chuyển thời gian ve dang giay
 * */
//$dateString = "2023-10-15 15:30:00";
//$timestamp = strtotime($dateString);

/*
 * mktime: tao ra thoi gian dang giay tu cac gia tri ngay, thang, nam
 * */
//$timestamp = mktime(16, 30, 0, 12, 25, 2024);
//echo date("Y-m-d H:i:s", $timestamp);

// checkdate: kiem tra xem mot ngay co hop le hay khong'
// checkdate(thang, ngay, nam)
//$month = 2;
//$day = 29;
//$year = 2024;
//checkdate($month, $day, $year);

// diff cho biet khoang cach gia 2 div
//$datetime1 = DateTime::createFromFormat('d/m/Y', '28/12/2021');
//$datetime2 = DateTime::createFromFormat('d/m/Y', '5/5/2011');
//$dateinterval = $datetime2->diff($datetime1);

/*
 * class DateTime chua thong tin ve thoi gian va cac chuc nang lien quan den tinh toan, chuyen doi
 *
 * */
//$datetime = new DateTime('now');
//echo date_format($datetime, "Y-m-d");

// Them va bot khoang cach thoi gian giua 2 ngay
// dung ham add hoac sub
$ngaysinh = DateTime::createFromFormat('d/m/yy', '20/11/1999');
$ngaysinh->add(new DateInterval('P20Y5M3D'));

echo $ngaysinh->format('d/m/Y');
