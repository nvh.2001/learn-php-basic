<?php

/*
 * Break Kết thúc vòng lặp ngay lập tức khi nó được gọi
 * */
//for ($i = 0; $i < 10; $i++) {
//    echo $i . "\n";
//    if ($i === 5) {
//        break;
//    }
//}
//echo "Kết thúc vòng lặp.\n";

/*
 * Continue: Bỏ qua điều kiện và tiếp tục chạy vòng lặp tiếp theo
 * */
for ($i = 0; $i < 10; $i++) {
    if ($i % 2 === 0) {
        continue;
    }
    echo $i . "\n";
}
echo "Kết thúc vòng lặp.\n";