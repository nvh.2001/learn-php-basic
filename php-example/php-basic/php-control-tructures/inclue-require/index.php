<?php

/*
 * Include va require đều dung để import một file vào một file khác
 * + Include:
 * - Nếu tệp không tồn tại thì sẽ đưa ra cảnh báo và tiếp tục chạy chương trình
 * + Require
 * - Nếu tệp không tồn tại thì sẽ  không đưa ra cảnh báo và dung chạy chương trình
 * */
include 'include.php';
require 'require.php';

echo "$include";

echo "$require";
