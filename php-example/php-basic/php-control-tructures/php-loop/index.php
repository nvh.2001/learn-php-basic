<?php

/*
 * While
 * + Kiểm tra điều kiện trước khi lặp lại
 * + Xử lý các trường hợp với điều kiện không biết trước về số lần lặp lại
 * */
//$i = 0;
//while ($i < 6) {
//    echo $i;
//    $i++;
//}

/*
 * do-while
 * + Nó sẽ thực hiện khối lệnh ít nhất 1 lần mới mới kiểm tra điều kiện lặp
 * */
//$result=1;
//$i = 5;
//do {
//    $result*=$i;
//    $i--;
//} while ($i > 0);
//echo $result;

/*
 * For
 * + Sử dụng khi biết trước số lần lặp cụ thể
 * */
//for ($i = 0; $i++ < 6; $i++) {
//    echo $i; // 1 3 5
//}

/*
 * Foreach
 * + Lặp qua từng phần tử trong array hoặc các thuộc tính trong một đối tượng
 * */
//$array = array("apple", "banana", "cherry");
//foreach ($array as $value) {
//    echo $value . "<br>";
//}
//
//$age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);
//foreach ($age as $key => $value) {
//    echo "Key=" . $key . ", Value=" . $value . "<br>";
//}



