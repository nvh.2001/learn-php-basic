<?php
// If - elseif - else
$diem = 4;

if ($diem >= 8) {
    echo "Hoc sinh gioi";
} elseif ($diem < 8 && $diem >= 6.5) {
    echo "Hoc sinh kha";
} else {
    echo "Hoc sinh Trung Binh";
}

//Switch - case
switch (true) {
    case $diem >= 8:
        echo "Hoc sinh gioi";
        break;
    case $diem < 8 && $diem >= 6.5:
        echo "Hoc sinh kha";
        break;
    case $diem < 6.5 && $diem > 4:
        echo "Hoc sinh Trung Binh";
        break;
    default:
        echo "Hoc sinh Yeu";
}
/*
 * - If - else
 * + Với các điều kiện phức tạp không cố định
 * + Số lượng điều kiện ít
 * - Switch - case
 * + Kiểm tra với nhiều giá trị cố định
 * + Có nhiều nhánh điều kiện
 * */

/*
 * Match
 * + Giống với switch
 * + So sánh nghiêm ngặt sử dụng ===
 * */
//$diem = 7;
//$loaiHocSinh = match (true) {
//    $diem >= 8 => "Hoc sinh gioi",
//    $diem < 8 && $diem >= 6.5 => "Hoc sinh kha",
//    $diem < 6.5 && $diem > 4 => "Hoc sinh Trung Binh",
//    default => "Hoc sinh Yeu",
//};
//
//echo $loaiHocSinh;


