<?php

/*
 * Function có thể tái sử dụng nhiều lần
 * + Đặt tên function bắt đầu với từ khóa function <ten function>
 * + Đặt tên function theo camelCase
 * */

function myFunction(): void
{
    echo "My Function";
}

myFunction();

/*
 * Tham số trong function
 * + Được viết sau dấu ngoặc đơn ngay sau tên hàm
 * + Function có thể khai báo nhiều tham số tùy ý
 * + Các cách truyền tham số: tham số mặc định và truyền tham số
 * */

// truyền tham số
function helloFrined($name): void
{
    echo "<p>Hello $name</p>";
}

helloFrined("Huy");

// tham số mặc định
function helloCountry($country = "Viet Nam"): void
{
    echo "<p>Hello $country</p>";
}
helloCountry();
helloCountry("Lao"); // Truyền giá trị vào tham số sẽ bị ghi đè tham số mặc định

/*
 * Function trả về giá trị
 * */
function tongHaiSo($a, $b)
{
    return $a + $b;
}

echo tongHaiSo(1, 2);

/*
 * Function với tham số có kiểu dữ lieu
 * */
function tongBaSo(int $a, float $b, float $c)
{
    return $a + $b + $c;
}

echo tongBaSo(1, 2.3, 3.5);
/*
 * tham trị và tham chiếu: Đều để dùng truyền dữ liệu vào function
 * + Tham trị
 * - Khi truyền giá trị vào function thì nó sẽ tạo ra một bản sao, giá trị cũ vẫn giữ nguyên
 * + Tham chiếu
 * - Khi truyền giá trị vào function thì nó sẽ biến đổi giá trị gốc
 * - Sử dụng dấu &
 * */
//function thamTri($number): void
//{
//    $number += 1;
//    echo "<p>Gia tri ben trong tham tri la: $number";
//}
//
//$giaTri = 5;
//thamTri($giaTri);
//echo "<p>Gia tri ben ngoai tham tri la: $giaTri</p>";

//function thamChieu(&$number)
//{
//    $number += 1;
//    echo "<p>Gia tri ben trong tham chieu: $number</p>";
//}
//
//$giaTri = 6;
//thamChieu($giaTri);
//echo "<p>Gia tri ben ngoai tham chieu la: $giaTri</p>";

/*
 * Arrow function
 * Sử dụng từ khóa fn để định nghĩa một function
 * */
//$add = fn($a, $b) => $a + $b;
//echo $add(5, 3);


