<?php

/*
 * Constants dung để lưu giá trị cố định và không thể bị thay đổi
 * Constants Có phân biệt chữ hoa và chữ thường
 * Cách khai báo: define(name, value, boolean) hoặc const
 * */

define("Animal", "dog");
echo Animal;
echo "<p></p>";

const Vehicle = "Bus";
echo "Day la phuong tien: " . Vehicle;