<?php

// Int
$Integer = 5;
echo $Integer;

// String
$String = "Day la Huy";
echo "<p>$String</p>";

// Float
$Float = 22.5;
echo "<p>$Float</p>";

// Boolean
$Boolean = true;

//Array
$array = array("1", 2, 3, "4");
var_dump($array);

// Object
class Mobile
{
    private $color;
    private $heDieuHanh;

    public function __construct($color, $heDieuHanh)
    {
        $this->color = $color;
        $this->heDieuHanh = $heDieuHanh;
    }

    public function info()
    {
        return "<p>Mau la: {$this->color}, He dieu hanh la: {$this->heDieuHanh}</p>";
    }
}

$xiaomi = new Mobile("Trang", "Android");
echo $xiaomi->info();

// Null: Một biến được tạo mà không có giá trị hoặc sẽ được gán bằng NULL
$A = "";
var_dump($A);

// Enum: Kiểu dữ liệu đặc biệt, định nghĩa một tập hợp giá trị cố định
enum Status
{
    case Pending;
    case Approved;
    case Rejected;
}
print_r(Status::Rejected);
