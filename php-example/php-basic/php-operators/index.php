<?php

$a = 5;
$b = 5;

// Toán tử số học
$cong = $a + $b;
$tru = $a - $b;
$chia = $a / $b;
$nhan = $a * $b;
$chiaLayPhanDu = $a % $b;
$luyThua = $a ** $b;
// Toán tử gán
$x = $b;
$a += $b; // a = a + b
$a -= $b; // a = a - b
$a *= $b; // a = a * b
$a /= $b; // a = a / b
$a %= $b; // a = a % b
// Toán tử so sánh
$soSanhMot = $a == $b ? "true" : "false"; // So sanh ve gia tri
$soSanhHai = $a === $b ? "true" : "false"; // So sanh gia tri va kieu du lieu
$soSanhBa = $a != $b ? "true" : "false"; // So sanh khac
$soSanhBon = $a <> $b ? "true" : "false"; // So sanh khong bang
$soSanhNam = $a !== $b ? "true" : "false"; // So sanh khong giong nhau
$soSanhSau = $a > $b ? "true" : "false"; // So sanh lon hon
$soSanhBay = $a < $b ? "true" : "false"; // So sanh be hon
$soSanhTam = $a >= $b ? "true" : "false"; // So sanh lon hon hoac bang
$soSanhChin = $a <= $b ? "true" : "false"; //So snah be hon hoac bang
//echo $soSanhNam;

// Toán tử tăng giảm

//$x = 7;
//echo $y = ++$x + 5; //Sẽ tăng $x lên một đơn vị luôn khi thực hiện phép toán khác trong cùng 1 câu lệnh

//$x = 7;
//echo $y = $x++ + 5; // Su dung gia tri hien tai cua $x va sau do moi tang len mot don vi

// Toán tử logic
if ($a && $b) { // Ca 2 cung phai dung
    echo "true";
} else {
    echo "false";
}
if ($a || $b) { // Mot trong hai phai dung
    echo "true";
} else {
    echo "false";
}
if (!$a) { // Khac a
    echo "true";
} else {
    echo "false";
}

// Toán tử gán có điều kiện
$ganCoDieuKienMot = $a == $b ? "true" : "false";
$ganCoDieuKienHai = $a ?? "Khong co gi";

// Toan tu gan String
//$a = "Hello ";
//$b = $a . "World!";