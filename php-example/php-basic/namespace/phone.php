<?php

namespace SmartPhone;

class Iphone
{
    private string $name = 'Iphone 11';

    public function getName(): string
    {
        return $this->name;
    }
}