<?php

/*
 * Array Là một biến có thể lưu nhiều giá trị
 * Khai báo mảng bằng [] hoặc array()
 * */

/*
 * Mảng đa chiều
 * */
//$profile = array(
//    'name' => 'Huy',
//    'year' => 2001,
//    'hobbies' => array(
//        array(
//            'game_1' => 'FO4',
//            'game_2' => 'CSGO'
//        )
//    ),
//    'city' => 'HN'
//);

/*
 * Mảng không tuần tự
 * */
//$car = array("brand" => "Ford", "model" => "Mustang", "year" => 1964);
//
//foreach ($car as $x => $y) {
//    echo "$x: $y <br>";
//}

$cars = ["key" => "Volvo", "BMW", "Toyota", "Volvo"];

// Access item
//echo $cars[0];

//Add item
//$cars[] = "Merc";
//$cars['years'] = "2001";

// Update item
//$cars[1] = "CX5";

// Remove item
//unset($cars[1]);
//var_dump($cars);

/*
 * Các hàm làm việc với array
 * */
// in_array: Kiểm tra xem mảng có tồn tại value hay không
//var_dump(in_array("Huy", $cars));

// is_array: Kiểm tra một biến có phải là mảng hay không
//print_r(is_array($cars));

// count: Đếm số phần tử
//echo count($cars);

// array_value: đưa mảng về mảng tuần tự
//print_r(array_values($cars));

// array_key: trả về một mảng tuần tự với giá trị key
//print_r(array_keys($cars));

// array_pop: Trả về phần tử cuối cùng của mảng
//print_r(array_pop($cars));

// array_push: thêm một phần tử vào cuối mảng và trả về số lượng phần tử trong mảng
//print_r(array_push($cars, 'LX'));

// array_shift: Xóa phần tử đầu mảng và trả về phần tử vừa xóa
//print_r(array_shift($cars));

// array_unshift: Thêm một hoặc nhiều phần tử vào đầu mảng và trả về số lượng phần tử
//print_r(array_unshift($cars, "VINFAST", "Lenovo"));

// sort: Sắp xếp lại mảng theo chiều tăng dần
//print_r(sort($cars));

// array_merge: gộp mảng
//$a = ['a', 'b', 'c'];
//$b = ['e', 'f', 'g', 'h'];
//$c = ['z', 'y'];
//print_r(array_merge($a, $b, $c));

// array_slice: Lấy ra item muốn bắt đầu trong mảng
// array_slice($array, $start, $length, true or false)
//print_r(array_slice($cars, 2, 2, true));

// array_unique: Loại bỏ các item trùng nhau trong mảng
//print_r(array_unique($cars));

// serialize: encode một array thành một chuỗi được mã hóa
//print_r($a = serialize($cars));

//unserialize: decode một array đã được encode
//print_r(unserialize($a));

// array_diff: so sanh mang, tra ve cac phan tu dau tien trang mang thu nhat, loai bo cac gia tri trung lap

