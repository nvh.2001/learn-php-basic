<?php

/*
 * Sử dụng dấu " " và ' ' để tạo ra một chuỗi
 * Sử dụng dấu " " để truyền biến vào trong một chuỗi
 * */
$str = "đang ăn tối";
echo "Huy $str ";

/*
 * Muốn hiện thị dấu nháy " " trong một chuỗi phải bọc nó vào trước dấu \
 * */
//echo "Huy \"đang ăn tối\" ";

/*
 * Các hàm làm việc với string
 * */

$string = "Day la mot string";
// addcslashes: thêm các dấu \ phía trước ký tự trong chuỗi
//echo (addcslashes("Huy", 'a...z'));

//addslashes: thêm dấu \ trước các ký tự như ' "
//echo addslashes ("Huy's");

// explode: Chuyển chuỗi thành mảng
//print_r(explode(" ", $string));

// implode: Chuyển mảng thành chuỗi

//strlen: Đếm số ký tự trong chuỗi
//echo strlen($string);

// str_word_count: Đếm số từ trong chuỗi
//echo str_word_count($string);

// str_repeat: Lặp chuỗi
//echo str_repeat($string, 4);

// str_replace: Tìm kiếm vào thay thế một chuỗi
// str_replace(chuỗi tìm, chuỗi thay thế, chuỗi ban đầu)
//echo $str = str_replace("string", "int", $string);

// md5: Mã hóa một chuỗi
//echo md5($string);

// strstr: Tách chuỗi từ một ký tự cho trước đến hết chuỗi
// strstr(chuỗi ban đầu, ký tự ban đầu)
//echo strstr($string, 'mot');

// strtolower: biến đổi một chuỗi thành chữ thường

// strtoupper: biến đổi một chuỗi thành chữ hoa

// trim: Loại bỏ các dấu không mong muốn ở đầu và cuối chuỗi
//echo trim($string);

// json_decode: Chuyển một json sang array hoặc object
// json_decode($json, true or false)
//$json_string = '{"name": "John", "age": 30, "city": "New York"}';
//$decoded_data = json_decode($json_string);
//
//var_dump($decoded_data);

// json_encode: chuyển một array hoặc object sang json
//$data = array("name" => "John", "age" => 30, "city" => "New York");
//echo json_encode($data);