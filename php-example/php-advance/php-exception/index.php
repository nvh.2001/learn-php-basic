<?php

/*
 * *Exception
 * + Để xử lý các lỗi phát sinh trong quá trình thực thi chương trình
 * + Exception là một class chứa các thông tin về error xảy ra
 * + Các hàm làm việc với exception
 * - getMessage(): Trả về message mô tả khi throw exception ném ra
 * - getFile(): Trả về đường dẫn của file
 * - getLine(): Trả về số dòng
 * * Throw new exception
 * + Là sẽ ném ra một exception và đoạn code đó sẽ không dc thực thi
 * + Nếu kh bắt throw exception ct sẽ bị dừng ngay lập tuc
 * * Try - catch
 * + Dùng để bắt các throw exception và tránh để dừng chạy chương trình
 * */
function tinhSoChia($soChia, $soBiChia): float|int
{
    if ($soBiChia == 0) {
        // Ném một ngoại lệ
        throw new Exception("So Bi Chia Khong Duoc Bang 0");
    }
    return $soChia / $soBiChia;
}

try {
    echo tinhSoChia(5, 0);
} catch (Exception $ex) {
    $message = $ex->getMessage();
    echo $file = $ex->getFile();
    $line = $ex->getLine();
} finally {
    echo "Hoan thanh xong";
}

