<?php

/*
 * fopen(): Mở file và tạo file mới khi chưa tồn tại
 * + r : mở file và chỉ đọc
 * + r+: mở tệp để đọc và ghi
 * + w: chỉ để ghi nội dung. Xóa nội dung tệp nếu tệp đã tồn tại, Tạo tệp mới nếu tệp không tồn tại.s
 * + w+: Mở tệp để đọc và ghi. Xóa nội dung tệp nếu tệp đã tồn tại. Tạo tệp mới nếu tệp không tồn tại
 * + x: Tạo một tập tin mới chỉ để ghi
 * + x+: Tạo một tập tin mới để đọc/ghi
 * */
$myfile = fopen("test.txt", "r");
$myFileWrite = fopen("test.txt", "w");

/*
 * Đọc file
 * + readfile()
 * + fread(ten_file, so_luong_muon_lay): đọc file khi file đó đã được fopen()
 * + fget(): dùng để đọc một dòng trong file
 * + feof(): dùng để lặp qua dữ liệu trong file
 * + fgetc(): dùng để đọc một ký tự đơn lẻ
 * */

//echo $readFile = readfile("test.txt");
//echo $fRead = fread($myfile, 6);
//echo $fGet = fgets($myfile);
//while (!feof($myfile)) {
//    echo fgetc($myfile) . " ";
//}
//fclose($myfile);

/*
 * Ghi file
 * + fwrite(): để ghi dữ liệu vào file khi đã mở file, nếu tồn tại dữ liệu thì sẽ bị ghi đè
 * + file_put_contents(): ghi dữ liệu vào tệp mà không cần mở, ghi và đóng tệp
 * - ghi nối tiếp vào trong file sử dụng FILE_APPEND
 * */
$textTest = "Day la file chi test 3";
//fwrite($myFileWrite, $textTest);
//file_put_contents("test.txt", $textTest,FILE_APPEND);

