<?php
// Đều là các biến siêu toàn cục
/*
 * $GLOBALS: được sử dụng để truy cập các biến toàn cục từ bất kỳ phạm vi nào
 * */
//$x = "Hello";
//
//function helloWorld()
//{
//    echo $GLOBALS['x'];
//}
//
//helloWorld();

/*
 * $_SERVER: là một mảng chứa các thông tin môi trường và máy chủ web
 * */
//echo $_SERVER['SERVER_NAME']; // Server đang chạy
//echo $_SERVER['DOCUMENT_ROOT']; // thư mục gốc chứa dự án
//echo $_SERVER['REQUEST_URI']; // đường dẫn URL
//echo $_SERVER['REMOTE_ADDR']; // dia chi IP

/*
 * $_GET: để truyền dữ liệu từ một trang web tới máy chủ thông qua URL
 * + Thông qua các param bắt đầu bằng dấu ? và phân biệt với nhau bằng dấu &
 * */
//echo 'Hello ' . $_GET["name"] . '!';

/*
 * $_POST: Gửi dữ liệu lên server dưới dạng ẩn thông qua HTTP Header
 * + Được gửi thông qua các form
 * */
//$username = $_POST['txtUserName'];
//$password = $_POST['txtPassword'];
//echo "<p>UserName : " . $username . "</p>";
//echo "<p>Password : " . $password . "</p>";

/*
 * $_FILES: là một cách để truy cập thông tin về các tệp được tải lên với phương thức POST enctype="multipart/form-data"
 * */
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
//    $file_name = $_FILES['file']['name']; // Lấy tên của tệp được tải lên
//    $file_type = $_FILES['file']['type']; // Lấy loại MIME của tệp
//    $file_size = $_FILES['file']['size']; // Lấy kích thước của tệp
//    $file_tmp_name = $_FILES['file']['tmp_name']; // Lấy đường dẫn tạm thời của tệp trên máy chủ
//
//    echo $file_name;
//}

/*
 * $_REQUEST lấy dữ liệu gửi đến máy chủ từ một yêu cầu HTTP thông qua các form
 * */
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
//    $name = $_REQUEST['txtUserName'];
//    $email = $_REQUEST['txtPassword'];
//    echo $name;
//}

/*
 * $_COOKIE:
 * + Cookie là một tập tin nhỏ được lưu trữ trên máy tính của người dùng khi họ truy cập trang web
 * + Nó chứa các thông tin về người dùng và trang web
 * + khi đóng trình duyệt thì cookie vẫn tồn tại và tồn tại bao lâu là do người dùng set.
 * VD:
 * + lưu trữ tên người dùng và mật khẩu để người dùng không cần nhập lại mỗi khi truy cập trang web
 * + sử dụng để theo dõi số lần người dùng truy cập trang web
 * */
// Tạo mới cookie
//setcookie("username", "admin", time() + 30 * 24 * 60 * 60);
//// Update cookie
//setcookie("username", "user");
//// Delete cookie
//setcookie("username", "", time() - 3600);
//if (!isset($_COOKIE["username"])) {
//    echo "Cookie đã được xóa";
//}

/*
 * Session
 * + dùng để lưu trữ thông tin như thông tin người dùng từ trang này sang trang khác
 * + Khi bắt đầu một session, php sẽ tạo ra một id gắn với cookie, để định danh người dùng trên máy chủ
 * VD: làm chức năng đăng nhập, giỏ hàng
 * */