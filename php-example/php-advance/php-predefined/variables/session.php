<?php
// bắt đầu session
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Session</title>
</head>
<body>
<?php
// Tạo biến session
$_SESSION["user_id"] = "1";
$_SESSION["username"] = "Huy";
// Hien thi
echo $_SESSION['username'];
// Update session
$_SESSION["username"] = "viettuts.vn";
echo $_SESSION['username'];
// Delete session
session_unset();
session_destroy();
print_r($_SESSION);
?>
</body>
</html>