<?php

/*
 * Error: Là vấn đề xảy ra trong quá trình thực thi các tệp lệnh, khiến ct dừng thực thi
 * + Các loại lỗi cơ bản trong php là:
 * - Parse: Lỗi cú pháp
 * - Fatal: xảy ra khi một hàm chưa được gọi hoặc bị lỗi
 * - Warning: Là một cảnh báo và không dừng thực thi chương trình, xảy ra khi yêu cầu một file nào đó bị lỗi
 * - Notice:Là một cảnh báo và không dừng thực thi chương trình, khi truy cập một biến chưa được khai báo
 * */
// Parse Error
//$x = "x"
//echo $x;

//Fatal Error
//function tongHaiSo($a, $b)
//{
//    echo $a + $b;
//}
//tongHaiSo(2,3);
//diff();

// Warning Error
//$x = "Hello all!";
//include("gochocit.php");
//echo $x;

// Notice Error
//$x = "gochocit.com";
//echo $x;
//echo $y;

/*
 * Xử lý error trong php
 * - die(): in thông báo lỗi và không dừng chương trình
 * - exit(): Kết thúc ngay lập tức thực thi ct
 * */

//$a = 5;
//if($a > 10) {
//    die("La so lon hon 10");
//} else {
//    exit("Abcd");
//}

/*
 * Custom Error với set_error_handler: kiểm soát lỗi và quản lý lỗi
 * ten_error(level_error, message, file, line)
 * */
//function customError($errno, $errstr, $errfile, $errline) {
//    echo "<b>Error:</b> [$errno]: $errstr <b>in</b> $errfile <b>at line</b> $errline";
//}
//set_error_handler("customError");
//echo($test);

/*
 * trigger_error: Tạo ra một thông báo lỗi do người dùng xác định
 * trigger_error(message, loai_error)
 * */
//$test=-1;
//if ($test<0) {
//    trigger_error("Value must be 0 or above", E_CORE_ERROR);
//}

/*
 * error_log: ghi log cho các error
 * error_log(message, type_error, duong dan file)
 * + Type error có 3 kiểu phải là 1 integer
 * - 0: file đã cấu hình trước đó
 * - 1: gửi qua mail
 * - 3: gửi tới một file cố định
 * */
//function customError($errno, $errstr, $errfile, $errline)
//{
//    echo "<b>Error:</b> [$errno]: $errstr <b>in</b> $errfile <b>at line</b> $errline";
//    error_log("Chuong trinh bi loi that nay!\n", 1, "huy.ngo.tda@gmail.com");
//}
//
//set_error_handler("customError");
//echo($test);