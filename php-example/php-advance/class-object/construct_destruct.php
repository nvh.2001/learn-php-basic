<?php

/*
 * Construct
 * + là một phương thức khi khởi tạo một đối tượng thì nó luôn được gọi kèm theo
 * */


/*
 * Destruct
 * + sẽ được gọi khi class đó được hủy
 * + dùng để giải phóng tài nguyên của một class
 * + trong một class có thể có hoặc không có phương thức hủy
 * + Nó sẽ được gọi ở cuối mỗi file
 * */

class Test
{
    public function __construct()
    {
        echo "Day la ham khoi tao";
    }

    public function test()
    {
        echo "Test success";
    }

    public function __destruct()
    {
        echo "Day la ham huy";
    }


}

$testSuccess = new Test();
$testSuccess->test();
