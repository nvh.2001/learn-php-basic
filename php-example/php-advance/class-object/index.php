<?php

/* Class
 * Các class được bắt đầu bằng từ khóa class
 * Class là một khuôn mẫu dùng để tạo ra các đối tượng
 * Class bao gồm 2 thành phần chính là: thuộc tính và phương thức
 * */

/* Object
 * + Là một đối tượng, nó đại diện cho một phiên bản cụ thể của một lớp
 * + Nó được khởi tạo thông qua từ khóa new
 * + Nó chứa dữ liệu cụ thể và thực hiện các phương thức của lớp
 * */
class Test
{
    public $myName = "Huy";
    public $age;

    public function __construct($age, $myName)
    {
        $this->age = $age;
        $this->myName = $myName;
    }

    public function getMyName(): void
    {
        echo $this->myName;
    }
}

$test1 = new Test(12, "Huy");
$test1->getMyName();
/*
 * Từ khóa final: ngăn chặn sự kế thừa và override đối với các lớp và phương thức
 * */
//final class Parent
//{
//    public function sayHello() {
//        echo "Hello from FinalClass\n";
//    }
//}
//
//class ParentPerson
//{
//    public final function getName()
//    {
//        echo "Name Parent Person";
//    }
//}