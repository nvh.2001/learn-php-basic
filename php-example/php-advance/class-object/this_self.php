<?php

/*
 * Dùng để tham chiếu tới các thuộc tính hoặc hàm của lớp
 * */

/*
 * Trong đối tượng
 * Self sẽ tham chiếu đến class khai báo nó, làm mất tính đa hình
 * This sẽ tham chiếu đến tối tượng hiện tại, thể hiện tính đa hình
 * */
//class DoAn
//{
//    public function classType()
//    {
//        echo "Đây là DoAn";
//    }
//
//    public function echoClassSelf()
//    {
//        self::classType();
//    }
//
//    public function echoClassThis() {
//        $this->classType();
//    }
//}
//
//class Pho extends DoAn
//{
//    public function classType()
//    {
//        echo "Đây là Pho";
//    }
//}
//
//$pho = new Pho();
//$pho->echoClassThis();
//$pho->echoClassSelf();

/*
 * Khi truy cập các thuộc tính tĩnh
 * + This: Không thể truy cập và thay đổi giá trị của thuộc tính tĩnh
 * + Self: Có thể truy cập và thay đổi giá trị của thuộc tính tĩnh
 * */
/*
 * Sử dụng trong phương thức tĩnh
 * + This: Không sử dụng $this trong phương thức static để truy cập các thuộc tính
 * + Self: Có thể sử dụng trong phương thức static và có quyền truy cập các thuộc tính
 * */
//class DoAn
//{
//    public static $type;
//
//    public static function echoClass() {
//        $this->type = 'Do An';
//    }
//}
//$doan = new DoAn();
//$doan->echoClass();

//class DoAn
//{
//    public static $type;
//
//    public static function echoClass() {
//        echo self::$type = 'Do An';
//    }
//}
//$doan = new DoAn();
//$doan->echoClass();

/*
 * Khi truy cập các phương thức tĩnh
 * + Cả this và self đều truy cập được các phương thức tĩnh
 * */
//class DoAn
//{
//    public static function echoClass()
//    {
//        echo 'Do An';
//    }
//
//    public function useSelf()
//    {
//        self::echoClass();
//    }
//}
//$doan = new DoAn();
//$doan->useSelf();

//class DoAn
//{
//    public static function echoClass()
//    {
//        echo 'Do An';
//    }
//
//    public function useSelf()
//    {
//        $this->echoClass();
//    }
//}
//$doan = new DoAn();
//$doan->useSelf();



