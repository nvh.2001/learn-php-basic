<?php

/*
 * + Là một cơ chế tái sử dụng mã trong nhiều lớp
 * + Cho phép sử dụng các phương thức bất cứ đâu không cần phải kế thừa
 * */
trait LogMessage
{
    public function getMessage()
    {
        echo "Get message";
    }
}

class Login
{
    use LogMessage;

    public function logLogin()
    {
        $this->getMessage();
    }
}

$login = new Login();
echo $login->logLogin();