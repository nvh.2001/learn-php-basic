<?php

/*
 * Static được sử dụng để khai báo các thuộc tính và phương thức tĩnh
 * + Nó thuộc về lớp chứ không phải một đối tượng cụ thể
 * + Gọi chúng bên trong lớp thông qua tên class hoặc self
 * */
class ConNguoi
{
    public static $name = 'Vũ Thanh Tài';

    public static function getName()
    {
        echo self::$name;
    }

    public static function showAll()
    {
        echo ConNguoi::getName();
    }
}
ConNguoi::showAll();

