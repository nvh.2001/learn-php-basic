<?php

/*
 * Dong goi
 * +  giúp bảo vệ dữ liệu của một đối tượng
 * + Thể hiện thông qua các từ khóa: public, protected, private
 * - public: có thể được truy cập từ bất kỳ đâu
 * - protected: chỉ có thể được truy cập từ bên trong lớp và các lớp con
 * - private: chỉ có thể được truy cập từ bên trong chính lớp đó
 *   ( muốn truy cập các thuộc tính trong private phải thông qua các phương thức public)
 * */

class Person
{
    public $name;
    protected $age;
    private $gender;
}

