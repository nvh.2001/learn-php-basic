<?php

/*
 * Interface
 * + Là một khuôn mẫu mà các đối tượng implement interface đó sẽ thực thi
 * + Interface có thể kế thừa lẫn nhau
 * + Một class có thể implement nhiều interface\
 * + có thể khai báo được hằng nhưng không thể khai báo biến.
 * + Chỉ được phép định nghĩa các phương thức chứ không được phép viết code xử lý
 * */
interface DongVat
{
    public function getName();
}

interface ThucAn extends DongVat
{
    public function loaiThucAn();
}

class ConTrau implements DongVat, ThucAn
{

    public function getName()
    {
        echo "Day la con Trau";
    }

    public function loaiThucAn()
    {
        echo "An co";
    }
}

$conTrau = new ConTrau();
$conTrau->getName();
$conTrau->loaiThucAn();