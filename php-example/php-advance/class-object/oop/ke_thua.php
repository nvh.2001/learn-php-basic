<?php
/*
 * - Kế thừa
 * + Các class có thể kế thừa lẫn nhau
 * + Class con có thể sử dụng các thuộc tính và phương thức của lớp cha
 * + Sử dụng thông qua từ khóa extend
 * */
class Animal
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function speak(): void
    {
        echo "$this->name is making a sound.";
    }
}

// Lớp con kế thừa từ lớp cha Animal
class Dog extends Animal
{
    public function speak(): void
    {
        echo "$this->name is barking.";
    }
}

// Tạo đối tượng của lớp Dog
$dog = new Dog("Buddy");
$dog->speak(); // Kết quả: Buddy is barking.