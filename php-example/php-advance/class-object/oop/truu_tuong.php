<?php

/*
 * Tính trừu tượng
 * + Tạo ra các lớp trừu tượng và phương thức trừu tượng
 * + Chỉ được phép khai báo abstract method không được phép viết code xử lý
 * + Các lớp con extend một abstract class sẽ phải override lại abstract method
 * */

abstract class Animal
{
    abstract public function sound();
}

class Dog extends Animal
{
    public function sound()
    {
        echo "Cho keu gau gau";
    }
}

class Cat extends Animal
{
    public function sound()
    {
        echo "Meo keu meo meo";
    }
}

$dog = new Dog();
$dog->sound();
$cat = new Cat();
$cat->sound();