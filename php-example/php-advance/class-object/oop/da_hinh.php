<?php

/*
 * Đa hình
 * + Các đối tượng khác nhau thực thi một chức năng giong nhau, nhung cho ra ket qua khac nhau
 * + Thể hiện qua việc override các phương thức của lop cha hoac thong qua interface
 * */

class Animal
{
    public function speak()
    {
        echo "Animal makes a sound.\n";
    }
}

class Dog extends Animal
{
    public function speak()
    {
        echo "Dog barks.\n";
    }
}

class Cat extends Animal
{
    public function speak()
    {
        echo "Cat meows.\n";
    }
}
$animal = new Animal();
$animal->speak();
$dog = new Dog();
$dog->speak();
$cat = new Cat();
$cat->speak();